/*!
 * Custom Redis module (needs Redis 6 or newer) for the GEMMA/GNAO project, augmenting
 * the regular keyspace notifications in order to obtain not only a notification that
 * a string was SET, but also its value.
 *
 * This is an essential feature, as capturing regular keyspace notifications we need
 * to read back the value after the fact. Given that this is not atomic, there's always
 * the risk of reading a value that has changed since being notified.
 */

#include "redismodule.h"
#include <string.h>

#define MODULENAME "gnaopublisher"
#define MODULEVERSION 1
#define PUBLISHERPREFIX "__gnao_set__"

int GnaoPublisher_NotifyCallback(RedisModuleCtx *, int, const char *, RedisModuleString *);

/*!
 *
 * This is the module initialization function. Called by Redis upon loading it.
 *
 * \param ctx the module context
 * \param argv optional arguments - unused here
 * \param argc argument count
 */

int RedisModule_OnLoad(RedisModuleCtx *ctx, RedisModuleString **argv, int argc) {
	if (RedisModule_Init(ctx, MODULENAME, MODULEVERSION, REDISMODULE_APIVER_1) == REDISMODULE_ERR)
		return REDISMODULE_ERR;

	RM_SubscribeToKeyspaceEvents(ctx, REDISMODULE_NOTIFY_STRING, GnaoPublisher_NotifyCallback);

	return REDISMODULE_OK;
}

/*!
 *
 * This is the string notification callback. It reads the modified key's value
 * and publishes is using a custom message.
 *
 * \param ctx the module context
 * \param type an integer indicating the type of event (string, list, hash, ...)
 * \param event a string describing which kind of event triggered (SET, expiration, ...)
 * \param key_name a RedisModuleString with the name of the key that was modified.
 *
 * We only register string events, so the type is irrelevant here.
 */

int GnaoPublisher_NotifyCallback(RedisModuleCtx *ctx, int type, const char *event, RedisModuleString *key) {
	// Ask Redis to deal with freeing memory up, and closing open keys.
	RedisModule_AutoMemory(ctx);


	// We're only concerned with SET events. SETRANGE added for flexibility.
	if (!strcmp(event, "set") || !strcmp(event, "setrange")) {
		size_t len;
		const char *raw_value;
		RedisModuleString *channel;
		RedisModuleString *value;

		// Grab the new value
		raw_value = RedisModule_StringDMA(RedisModule_OpenKey(ctx, key, REDISMODULE_READ),
				&len, REDISMODULE_READ);
		value = RedisModule_CreateString(ctx, raw_value, len);

		// The publishing channel has a common prefix, followed by the key
		channel = RedisModule_CreateStringPrintf(ctx, PUBLISHERPREFIX ":%s",
				RedisModule_StringPtrLen(key, NULL));

		RedisModule_PublishMessage(ctx, channel, value);
	}

	return REDISMODULE_OK;
}
