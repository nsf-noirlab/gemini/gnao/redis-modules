%define _prefix __auto__
%define gemopt opt
%define name redis-modules
%define version 1.1
%define release 1
%define repository gemini

%define debug_package %{nil}

Summary: Redis extension modules for Gemini
Name: %{name}
Version: %{version}
Release: %{release}.%{dist}.%{repository}
License: BSD
## Source:%{name}-%{version}.tar.gz
Group: Gemini
Source0: %{name}-%{version}.tar.gz
BuildRoot: /var/tmp/%{name}-%{version}-root
BuildArch: %{arch}
Prefix: %{_prefix}
## You may specify dependencies here
BuildRequires: gcc

%description
Extension modules for Redis related to Gemini's operation.

 - gnaopublisher: Extension to the regular keyspace notifications
                  that publishes not key:event but key:value

%prep
## Do some preparation stuff, e.g. unpacking the source with
%setup -n %{name}

%build
make -C src

%install
install -d %{buildroot}/%{_libdir}/redis-gemini
install -m 755 src/gnaopublisher.so %{buildroot}/%{_libdir}/redis-gemini

%clean
## Usually you won't do much more here than
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%dir %{_libdir}/redis-gemini
%{_libdir}/redis-gemini/gnaopublisher.so


%changelog
* Fri Oct 15 2021 Ricardo Cardenes <ricardo.cardenes@noirlab.edu> 0.1
- Initial release
