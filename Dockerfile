FROM redis:7.2 as builder

RUN apt-get update
RUN apt-get install -y gcc make
WORKDIR /tmp/work

COPY . .
RUN make -C src

FROM builder as runner

COPY --from=builder /tmp/work/src/gnaopublisher.so /usr/local/lib/
COPY --from=builder /tmp/work/redis.conf /usr/local/etc/

ENTRYPOINT ["/usr/local/bin/redis-server", "/usr/local/etc/redis.conf"]
EXPOSE 6739/tcp
